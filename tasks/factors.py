def factors(number):
    # ==============
    # Your code here
    factors = []
    for x in range(2, number):
        y = number % x
        if y == 0:
            factors.append(x)

    if not factors:
        print(number, "is a prime number")
    elif factors:
        return factors
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “[]” (an empty list) to the console
